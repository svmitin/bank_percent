# -*- coding: utf-8 -*-
"""Объявление URL"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from main.views import RootView, APIPercent
from django.contrib.auth.models import User, Group


admin.site.unregister(User)
admin.site.unregister(Group)
admin.autodiscover()
admin.site.enable_nav_sidebar = False

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RootView.as_view(), name='root'),
    path('getPercent/<int:people_category_id>/<int:home_category_id>', APIPercent.as_view(), name='get_percent'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
