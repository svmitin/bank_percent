from django.apps import AppConfig


class MainConfig(AppConfig):
    """Магазин"""
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main'
    verbose_name = 'Калькулятор процентных ставок'
