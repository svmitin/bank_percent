# -*- coding: utf-8 -*-
"""Импорт моделей в админ-панель"""
from django.contrib import admin

from .models import PeopleCategory, HomeCategory


@admin.register(PeopleCategory)
class PeopleCategoryAdmin(admin.ModelAdmin):
    """Представление для Категорий клиентов банка"""
    list_display = (
        'id', 'caption', 'percent', 'ctime'
    )
    list_display_links = ('id', 'caption',)
    list_filter = ('caption', 'percent', 'ctime')
    search_fields = ('caption',)
    list_editable = ('percent',)


@admin.register(HomeCategory)
class HomeCategoryAdmin(admin.ModelAdmin):
    """Представление для Категорий кредитуемого жилья"""
    list_display = (
        'id', 'caption', 'percent', 'ctime'
    )
    list_display_links = ('id', 'caption',)
    list_filter = ('caption', 'percent', 'ctime')
    search_fields = ('caption',)
    list_editable = ('percent',)
