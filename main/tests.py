# -*- coding: utf-8 -*-
"""Листинг, реализующий тестирование"""
from django.test import TestCase
from .models import PeopleCategory, HomeCategory


class MainTestClass(TestCase):

    @classmethod
    def setUpTestData(cls):
        """Настройка тестовой БД"""
        PeopleCategory.objects.create(caption='Сотрудник банка', percent=2)
        HomeCategory.objects.create(caption='Обычное жильё', percent=4)

    def test_main_page(self):
        """Проект возвращает главную страницу"""
        # Запрос главной страницы
        response = self.client.get('/')
        # Сравниваем полученный код HTTP с желаемым двухсотым
        self.assertEqual(response.status_code, 200)

    def test_get_percent_api_page(self):
        """Проверка корректности возвращаемой процентной ставки"""
        response = self.client.get('/getPercent/1/1')
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'percent': 6})
