# -*- coding: utf-8 -*-
"""Сущности. Категории клиентов банка, а также кредитуемого жилья"""
from django.db import models
from django.utils import timezone


class Category(models.Model):
    """Категории. Родительский класс"""
    caption = models.CharField('Название', max_length=90, unique=True)
    percent = models.IntegerField('Процент')
    ctime = models.DateTimeField('Запись создана', default=timezone.now)

    def __str__(self):
        return self.caption

    class Meta:
        # Класс является абстрактным (родительским),
        # не нужно создавать таблицу
        abstract = True


class PeopleCategory(Category):
    """Категории клиентов банка"""

    class Meta:
        ordering = ['percent']
        verbose_name = 'Клиент'
        verbose_name_plural = 'Категории клиентов банка'


class HomeCategory(Category):
    """Категории кредитуемого жилья"""

    class Meta:
        ordering = ['percent']
        verbose_name = 'Жилье'
        verbose_name_plural = 'Категории кредитуемого жилья'
