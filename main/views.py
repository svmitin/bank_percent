# -*- coding: utf-8 -*-
"""Представления страниц"""
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic.base import View

from .models import PeopleCategory, HomeCategory


class RootView(View):
    """Главная страница"""

    @staticmethod
    def get(request):
        """Метод GET. Получить страницу с формой"""
        # Извлечем из БД все категории заемщиков и жилья для отображения на главной странице
        people_categories = [p for p in PeopleCategory.objects.order_by('percent').all()]
        home_categories = [h for h in HomeCategory.objects.order_by('percent').all()]
        # Рендер и возвращение страницы. В качестве шаблона используем файл index.html
        context = dict(people_categories=people_categories,
                       home_categories=home_categories)
        return render(request, 'index.html', context=context)


class APIPercent(View):
    """Запрос на получение ответа банка"""

    @staticmethod
    def get(request, people_category_id: int, home_category_id: int) -> JsonResponse:
        """Метод GET. Запросить процентную ставку у банка"""
        # В качестве аргументов были получены id выбранных категорий населения и жилья
        # найдем их в БД
        people_category = PeopleCategory.objects.filter(id=people_category_id).first()
        home_category = HomeCategory.objects.filter(id=home_category_id).first()
        # Калькуляция
        percent = people_category.percent + home_category.percent
        return JsonResponse({'percent': percent}, status=200)
